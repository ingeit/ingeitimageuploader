import express from 'express';
import cors from 'cors';
import path from 'path';
import bodyParser from 'body-parser';
import Axios from 'axios';
const app = express();
const port = 3001;

app.use(bodyParser.json({ limit: '4mb', extended: true }))
app.use(bodyParser.urlencoded({ limit: '4mb', extended: true }))
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());
app.options('*', cors());

app.post('/test', (req, res, next) => {
    console.log(req.body);

    Axios.post('http://localhost:5734/preUpload', { ...req.body });
})

app.listen(port, function () { console.log('Serving HTTP on port ' + port); });