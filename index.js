import express from 'express';
import cors from 'cors';
import path from 'path';
import FormData  from 'form-data';
import bodyParser from 'body-parser';
import multer from 'multer';
import axios from 'axios';
const app = express();
const port = 5734; //JPEG
const url = `http://localhost:${port}/`;

app.use(bodyParser.json({ limit: '4mb', extended: true }))
app.use(bodyParser.urlencoded({ limit: '4mb', extended: true }))
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());
app.options('*', cors());

const storage = multer.diskStorage({
	destination: 'public/images',
	filename: (req, file, cb) => {
		var filetype = '';
		if (file.mimetype === 'image/gif') {
			filetype = 'gif';
		}
		if (file.mimetype === 'image/png') {
			filetype = 'png';
		}
		if (file.mimetype === 'image/jpeg') {
			filetype = 'jpg';
		}
		cb(null, 'image-' + Date.now() + '.' + filetype);
	}
});

const upload = multer({ storage });

app.post('/preUpload', (req, res, next) => {
  const bodyFormData = new FormData();
  bodyFormData.append('file', req.body.image); //e.target.files[0]
  axios({
    method: 'post',
    url: 'http://localhost:5734/upload',
    data: bodyFormData,
    config: { headers: {'Content-Type': 'multipart/form-data' }}
    })
    .then(function (response) {
        //handle success
        console.log('todo bien');
    })
    .catch(function (response) {
        //handle error
        console.log('error');
    });
})

app.post('/upload', upload.single('file'), (req, res, next) => {
	if(!req.file) {
	  res.status(500);
	  return next('err');
	}
	res.json({ fileUrl: `${url}/images/` + req.file.filename });
})

app.listen(port, function () { console.log('Serving HTTP on port ' + port); });